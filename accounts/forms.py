from django import forms
from accounts.models import User
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.conf import settings


class CreateUserForm(forms.ModelForm):
    repeat_password = forms.CharField(
        label="تکرار رمز عبور",
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password'}),
        strip=False
    )

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password', 'repeat_password']
        labels = {
            'username': 'نام کاربری',
            'password': 'رمز عبور',
            'repeat_password': 'تکرار رمز عبور',
            'first_name': 'نام',
            'last_name': 'نام خانوادگی',
            'email': 'ایمیل',
        }
        help_texts = {
            'username': "",
        }

    def __init__(self, *args, **kwargs):
        super(CreateUserForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

    def clean(self):
        super(CreateUserForm, self).clean()
        attrs = self.cleaned_data
        password = attrs.get('password', None)
        repeat_password = attrs.get('repeat_password', None)
        if password is None or repeat_password is None or password != repeat_password:
            raise ValidationError("لطفا پسورد ها را یکسان وارد نمایید.")
        return attrs

    def save(self, commit=True):
        data = self.cleaned_data
        data.pop('repeat_password')
        user = User(**data)
        user.set_password(data.get('password'))
        user.save()
        return user


class ContactsUsForm(forms.Form):
    title = forms.CharField(label='عنوان')
    email = forms.EmailField(label='ایمیل')
    text = forms.CharField(label='متن', widget=forms.Textarea, max_length=250, min_length=10)

    def __init__(self, *args, **kwargs):
        super(ContactsUsForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

    def send_msg(self):
        data = self.cleaned_data
        send_mail(subject=data.get('title'),
                  message=f'text: {data.get("text")} \n\n\n email: {data.get("email")} ',
                  from_email=settings.EMAIL_HOST_USER,
                  recipient_list=settings.RECIPIENT_LIST)


class EditProfileForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'gender', 'bio', 'avatar']
        labels = {
            'username': 'نام کاربری',
            'first_name': 'نام',
            'last_name': 'نام خانوادگی',
            'gender': 'جنسیت',
            'bio': 'زندگینامه',
            'avatar': 'عکس'
        }

        help_texts = {
            'username': "",
        }

    def __init__(self, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

    def clean_first_name(self):
        first_name = self.cleaned_data.get('first_name', '')
        if first_name == '':
            return self.instance.first_name
        return first_name

    def clean_last_name(self):
        last_name = self.cleaned_data.get('last_name', '')
        if last_name == '':
            return self.instance.last_name
        return last_name
