from django.urls import path
from accounts import views

app_name = 'account'
urlpatterns = [
    path('signup', views.signup_page_view, name='signup'),
    path('login', views.login_page_view, name='login'),
    path('logout', views.logout_page_view, name='logout'),
    path('contact-us', views.contact_us_page_view, name='contact_us'),
    path('success', views.success_page_view, name='success'),
    path('profile', views.profile_page_view, name='profile'),
    path('edit-profile', views.edit_profile_page_view, name='edit_profile'),
]
