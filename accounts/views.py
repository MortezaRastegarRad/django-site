from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from accounts.forms import CreateUserForm, ContactsUsForm, EditProfileForm


def signup_page_view(request):
    if request.method == "POST":
        form = CreateUserForm(data=request.POST)
        if not form.is_valid():
            return render(request, 'signup.html', {'form': form})
        form.save()
        return redirect('course:home')
    else:
        return render(request, 'signup.html', {'form': CreateUserForm()})


def login_page_view(request):
    if request.method == 'POST':
        username = request.POST.get('username', [''])[0]
        password = request.POST.get('password', [''])[0]
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return redirect('course:home')
        else:
            return render(request, 'login.html', {'errors': ['نام کاربری یا رمز عبور اشتباه است']})
    else:
        return render(request, 'login.html')


@login_required()
def logout_page_view(request):
    logout(request)
    return render(request, 'home.html')


def contact_us_page_view(request):
    if request.method == 'POST':
        form = ContactsUsForm(data=request.POST)
        if not form.is_valid():
            return render(request, 'contact.html', {'form': form})
        form.send_msg()
        return redirect('account:success')
    else:
        return render(request, 'contact.html', {'form': ContactsUsForm()})


def success_page_view(request):
    return render(request, 'success.html')


@login_required()
def profile_page_view(request):
    return render(request, 'profile.html')


@login_required()
def edit_profile_page_view(request):
    if request.method == 'POST':
        form = EditProfileForm(instance=request.user, data=request.POST, files=request.FILES)
        if not form.is_valid():
            return render(request, 'edit_profile.html', {'form': form})
        form.save()
        return redirect('account:profile')
    else:
        return render(request, 'edit_profile.html', {'form': EditProfileForm(instance=request.user)})
