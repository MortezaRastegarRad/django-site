from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    MALE = 'مرد'
    FEMALE = 'زن'
    gender = models.CharField(max_length=5, choices=(('0', MALE), ('1', FEMALE)))
    bio = models.TextField()
    avatar = models.ImageField(upload_to='images', null=True, blank=True)

    @property
    def display_gender(self):
        return self.get_gender_display()
