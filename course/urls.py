from django.urls import path
from course import views

app_name = 'course'
urlpatterns = [
    path('', views.home_page_view, name='home'),
    path('create/', views.CreateCoursePageView.as_view(), name='create_course'),
    path('panel/', views.panel_page_view, name='panel'),
]
