from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse
from django.views import generic

from course.forms import CreateCourseForm
from course.models import Course


def home_page_view(request):
    return render(request, 'home.html')


@login_required()
def panel_page_view(request):
    return render(request, 'panel.html', {'courses': Course.objects.all()})


class CreateCoursePageView(LoginRequiredMixin, generic.CreateView):
    queryset = Course.objects
    template_name = 'create_course.html'
    form_class = CreateCourseForm

    def get_success_url(self):
        return reverse('course:panel')
