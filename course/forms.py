from django import forms

from course.models import Course


class CreateCourseForm(forms.ModelForm):
    start_time = forms.TimeField(label='ساعت شروع', widget=forms.TimeInput(format='%H:%M'))
    end_time = forms.TimeField(label='ساعت پایان', widget=forms.TimeInput(format='%H:%M'))

    class Meta:
        model = Course
        fields = '__all__'
        labels = {
            'department': 'دانشکده',
            'name': 'نام',
            'group_number': 'شماره گروه',
            'course_number': 'شماره درس',
            'teacher_name': 'نام استاد',
            'start_time': 'ساعت شروع',
            'end_time': 'ساعت پایان',
            'first_day': 'روز اول',
            'second_day': 'روز دوم',
        }

    def __init__(self, *args, **kwargs):
        super(CreateCourseForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
        self.fields['second_day'].required = False
