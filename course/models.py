from django.db import models


class Course(models.Model):
    SATURDAY = 'شنبه'
    SUNDAY = 'یک شنبه'
    MONDAY = 'دو شنبه'
    THURSDAY = 'سه شنبه'
    WEDNESDAY = 'چهار شنبه'
    DAYS = (
        (0, SATURDAY),
        (1, SUNDAY),
        (2, MONDAY),
        (3, THURSDAY),
        (4, WEDNESDAY),
    )
    department = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    group_number = models.IntegerField()
    course_number = models.IntegerField()
    teacher_name = models.CharField(max_length=100)
    start_time = models.TimeField()
    end_time = models.TimeField()
    first_day = models.IntegerField(choices=DAYS)
    second_day = models.IntegerField(choices=DAYS, null=True, blank=True)

    def __str__(self):
        return f'{self.name}-{self.teacher_name}-{self.first_day}-{self.start_time}'

    @property
    def display_first_day(self):
        return self.get_first_day_display()

    @property
    def display_second_day(self):
        display = self.get_second_day_display()
        return '-' if display is None else display
